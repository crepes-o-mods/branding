# Branding

This mod removes all branding from the client/server, making Minecraft think it is unmodded. It doesn't remove every single modded information however (ex : Fabric API will still add some non vanilla lines to a crash report or informations on the F3 overlay).

This mod is NOT intended to use with cheats, or to protect you against anything should you have cheats installed. No support will be provided in this specific case.
